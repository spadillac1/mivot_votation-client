import axios from 'axios';
const URL = 'https://mivotbackend.herokuapp.com/api';
// const URL = 'http://localhost:5500/api';
const endpoints = {
  tokens: `${URL}/tokens`,
  votations: `${URL}/votations`,
  voters: `${URL}/voters`,
};

export const validateToken = (token) => {
  return axios.post(`${endpoints.tokens}/${token}`, {});
};

export const sendVote = (token, userId, vote) => {
  return axios.post(`${endpoints.voters}/${token}`, { vote, userId });
};
